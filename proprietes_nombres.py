# -*- coding: utf-8 -*-

import time as t
import datetime
from colorama import init 
from termcolor import colored
from pathlib import Path
from numba import jit, njit
from numba.typed import List

## Fonction-liste des diviseurs d'un nombre
@njit
def ListeDiv(nombre) :
    return [c for c in range(1,nombre+1) if nombre%c==0]

## Determine la propriete du nombre selon la somme de ses diviseurs : premier, parfait, deficient ou abondant
#@numba.jit("U10[:,:] (i4)", nopython=True, nogil=True)
@jit(nopython=True, nogil=True)
def Propriete(nombre,lst) :
    somme=0
    for elem in ListeDiv(nombre) :
            somme+=elem
    if nombre==somme-1 :
        lst[0]+=1
        return ('premier','red')
    elif 2*nombre==somme :
        lst[1]+=1
        return ('parfait','green')
    elif nombre>somme-nombre :
        lst[2]+=1
        return ('deficient','yellow')
    elif nombre<somme-nombre :
        lst[3]+=1
        return ('abondant','white')


## Fonctions qui serviront a inscrire les resultats dans le fichier texte
def WriteDateIntoFile(file):
    if boole :
            formattedDate = str(datetime.datetime.now().strftime('%d/%m/%Y-%Hh%M'))
            with open(file, 'a') as dateWriter:
                    dateWriter.write("===" + formattedDate + "===" + "\n")

def WriteIntoFile_FOR(file,text,nb) :
    if boole :
        with open(file,'a') as textWriter :
            textWriter.write(str(nb)+' ---> '+text+'\n')

def WriteIntoFile_STATS(file,text,nb,per) :
    if boole :
        with open(file,'a') as textWriter :
            textWriter.write(text+' : '+str(nb)+" (" +str(per)+" %) "+'\n')

def WriteTheEnd(file) :
    if boole :
        with open(file,'a') as textWriter :
            textWriter.write('\n'+'----------------------'+'\n')
        
init() # initialisation couleur

## initialisation du booleen servant a choisir d'ecrire ou non dans le fichier texte
boole=False
verif=input("Voulez-vous enregistrez les resultats dans un fichier texte ? (o/n) ")
if verif.lower()=='o' : boole=True

## Boucle programme
while True :
    try :
        ## Entree de la plage demandee
        l_tt=List([0,0,0,0])
        l=list(input("Plage à tester (nombres entiers non nuls) : ").split(" "))
        WriteDateIntoFile("sortie.txt")
        ti=t.time()
        
        ## Analyse
        for i in range(int(l[0]),int(l[-1])+1) :
            if i<=0 : print(i,"--> ERREUR")
            else :
                p=Propriete(i,l_tt)
                print(i,"est un nombre",colored(p[0],p[1])) # affichage du resultat
                WriteIntoFile_FOR("sortie.txt",p[0],i) # ecriture dans le fichier texte
        tf=t.time()

        ## Analyse statistique
        print('\n')
        print(colored("Premier :",'red'),l_tt[0],"(",round(l_tt[0]/(int(l[-1])-int(l[0])+1)*100,2),"%)")
        WriteIntoFile_STATS("sortie.txt","\nPremier",l_tt[0],round(l_tt[0]/(int(l[-1])-int(l[0])+1)*100,2))
        print(colored("Parfait :",'green'),l_tt[1],"(",round(l_tt[1]/(int(l[-1])-int(l[0])+1)*100,2),"%)")
        WriteIntoFile_STATS("sortie.txt","Parfait",l_tt[1],round(l_tt[1]/(int(l[-1])-int(l[0])+1)*100,2))
        print(colored("Deficient :",'yellow'),l_tt[2],"(",round(l_tt[2]/(int(l[-1])-int(l[0])+1)*100,2),"%)")
        WriteIntoFile_STATS("sortie.txt","Deficient",l_tt[2],round(l_tt[2]/(int(l[-1])-int(l[0])+1)*100,2))
        print(colored("Abondant :",'white'),l_tt[3],"(",round(l_tt[3]/(int(l[-1])-int(l[0])+1)*100,2),"%)")
        WriteIntoFile_STATS("sortie.txt","Abondant",l_tt[3],round(l_tt[3]/(int(l[-1])-int(l[0])+1)*100,2))

        WriteTheEnd("sortie.txt")
            
        print("\n")
        print("Exécuté en :",round(tf-ti,2), "s")
        print("---------------------------")
        t.sleep(1.5)

    except KeyboardInterrupt : #sortie du programme (CTRL+C)
        break
        
